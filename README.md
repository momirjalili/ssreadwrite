
#Task

Develop two applications which will work together to create users in an SQL database.

## Application 1

This application will read data from a CSV file and simultaneously publish or produce a message for each data row.

### Requirements

Each data row should be sent in a async/non-blocking way.
Maximum rows being processed at the same time should be limited to 100. The 101st row should wait till there is a room for it.
Data Format:

```{"email": "example@example.com", "fullname": "John Snow"}```

##Application 2


This application will subscribe to or consume messages from “Application 1” and simultaneously insert them into the database. Database Structure

###Requirements

The application should be able process multiple messages at the same time.
Maximum messages being processed at the same time should be limited to 50. The 51st message should wait till there is a room for it.
The application should insert a new row with the following data structure if there is no record with the same email address in the database.

```{"email": "example@example.com", "fullname": "John Snow", "times_received": 0} ```

The application should not insert a new row if the email address is already existing. Instead it should increase times_received value 1.
General requirements and notes
The task can be done with any programming language but preferably in Go or Python.
Any 3rd party queue can be used for message transport between 2 applications.
SQL file is created for PostgreSQL but it’s free to use any other relational database although the solution should not be relying on any specific database server or version.

####Bonus
Implement a mechanism to detect failures and recover and retry the messages.