import os

POSTGRES_DB=os.environ.get('POSTGRES_DB', 'simplesurance')
POSTGRES_USER=os.environ.get('POSTGRES_USER', 'simplesurance')
POSTGRES_PASSWORD=os.environ.get('POSTGRES_PASSWORD', 'simplesurance')

WRITER_SUB_LIMIT = 50
READER_PUB_LIMIT = 100
