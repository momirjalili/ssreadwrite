#!/bin/bash
set -e
echo "running init user db..."
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
CREATE USER simplesurance WITH PASSWORD 'simplesurance';
CREATE DATABASE simplesurance;
GRANT ALL PRIVILEGES ON DATABASE simplesurance TO simplesurance;
EOSQL

echo "created database and user in postgres."


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
CREATE TABLE users
(
    email VARCHAR(100) NOT NULL,
    fullname VARCHAR(100),
    times_received INTEGER NOT NULL
);

CREATE UNIQUE INDEX users_email_uindex ON users (email);
EOSQL


echo "created database table users..."
