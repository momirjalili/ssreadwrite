CREATE TABLE users
(
    email VARCHAR(100) NOT NULL,
    fullname VARCHAR(100),
    times_received INTEGER NOT NULL
);
CREATE UNIQUE INDEX users_email_uindex ON users (email);
