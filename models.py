from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False, unique=True)
    fullname = Column(String)
    times_received = Column(Integer, nullable=False, default=1)

    def __repr__(self):
        return "<User(email='%s', fullname='%s', times_received='%s')>" % (
                             self.email, self.fullname, self.times_received)
